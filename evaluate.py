# PyTorch imports:
import torch


def evaluate(model : torch.nn.Module, data_loader : torch.utils.data.DataLoader):
    """ Evaluates a given model using a given data loader. Evaluates
    the model according to the accuracy metric.

    Args:
        model (torch.nn.Module): The model that is to be evaluated
        data_loader (torch.utils.data.DataLoader): The data loader that contains
        the data the model will be evaluated with.      

    Returns:
        float: The accuracy of the model.
    """

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    
    # Setting the model to the evaluation mode:
    model.eval()
    
    # Moving the model to the desired device:
    model = model.to(device)
    
    num_correct = 0
    count = 0
    
    for x, y in data_loader:
        
        x, y = x.to(device), y.to(device)
        
        # Geenerating predictions from the model:
        y_pred = model(x.float())
        
        # Getting the predicted class for each image in the batch:
        _, label = torch.max(y_pred, axis=1)
        
         # Finding how many labels were predicted correctly:
        num_correct += (label==y).sum().item()
        
        # Using count variable becuase len(data_loader) * data_loader.batch_size does not
        # necessarly return the correct number of data points. Consider the case where
        # the number of data points is not divisible by the batch size.
        # y_pred.shape[0] Will give the number of images in a batch. This number will be
        # equal to the data_loader.batch_size for the most part, however, it will be 
        # different in the case where the number of images is not divisible by the batch
        # size. The last batch will have a number of images equal to the remainder of this
        # division operation.
        count += label.shape[0]
        
    accuracy = 100 * num_correct / count
    
    return accuracy