# PyTorch imports:
import torch
import torchvision
from torchvision.datasets import ImageFolder
from torchvision import utils

# OpenCV imports:
import cv2

# NumPy imports:
import numpy as np

# Matplotlib imports:
import matplotlib.pyplot as plt

# Other imports:
import os


TRANSFORMS = [lambda image : np.array(image),
              lambda image, threshold, max_val, type : cv2.threshold(image, threshold, max_val, type)[1],
              lambda image : cv2.bitwise_not(image),
              lambda image, size: cv2.resize(image, size),
              lambda image, kernel, iterations: cv2.dilate(image, kernel, iterations),
             ]

TRANSFORMS_PARAMS = [{},
                    {'threshold' : 200, 'max_val' : 255, 'type' : cv2.THRESH_BINARY},
                    {},
                    {'size' : (256, 256)},
                    {'kernel' : np.ones((3, 3), np.uint8), 'iterations' : 1},
                    ]


def transform_image(image, transforms, transforms_params):
    """Applies a series of transformations on a given image.

    Args:
        image (PIL): An input image to be transformed.
        transforms (List): A list containing the transformed to
        be applied on the given image.
        transforms_params (List): A list containing dictionaries
        that contain the parameter values for the transformations
        in the transforms list.

    Returns:
        N/A: An image transformed according to the given
        transformations.
    """
    for transform, params in zip(transforms, transforms_params):
        if len(params.values()) != 0:
            image = transform(image, **params)
        else:
            image = image = transform(image)
        
    return image

def plot_images(images : list, labels : list, subplots : tuple, fig_size=12, transform=None):
    """ Plots multiple images along with thier labels as titles.

    Args:
        images (list): A list images to plot.
        labels (list): The labels of the images to plot
        subplots (tuple): The number of subplots.
        fig_size (int, optional): The figure size of each image. Defaults to 12.
        transform (_type_, optional): The tranform that is to be applied
            on the plotted images. Defaults to None.
    """
    x, y = subplots
    fig, axarr = plt.subplots(x, y)
    
    fig.set_figwidth(fig_size)
    fig.set_figheight(fig_size)
    
    for image, label, ax in zip(images, labels, axarr.flatten()):
        if transform is not None:
            image = transform(image)
        
        ax.set_title(label)
        ax.imshow(image.squeeze())    


def visTensor(tensor, ch=0, allkernels=False, nrow=8, padding=1): 
    n,c,w,h = tensor.shape

    if allkernels: tensor = tensor.view(n*c, -1, w, h)
    elif c != 3: tensor = tensor[:,ch,:,:].unsqueeze(dim=1)

    rows = np.min((tensor.shape[0] // nrow + 1, 64))    
    grid = utils.make_grid(tensor, nrow=nrow, normalize=True, padding=padding)
    plt.figure( figsize=(nrow,rows) )
    plt.imshow(grid.numpy().transpose((1, 2, 0)))