# PyTorch imports:
import torch


# Other imports:
from tqdm import tqdm



def class_accuracies(model, data_loader, class_names : list):
    """Calculated the class accuracies of a given model.

    Args:
        model (torch.nn.Module): The model to be evaluated.
        data_loader (torch.utils.data.DataLoader): The data loader
            object that contains the data set that is to be tested
            on the model.
        class_names (List): A list containing the class names of the
            dataset.

    Returns:
        dict: A dictionary with keys being the class names and values
        being the accuracy for the corresponding class.
    """ 
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    
    model.eval()
    model.to(device)
    
    num_classes = len(class_names)
    
    confusion_matrix = torch.zeros(num_classes, num_classes)
    with torch.no_grad():
        for _, (inputs, classes) in tqdm(enumerate(data_loader)):
            inputs = inputs.to(device)
            classes = classes.to(device)
            outputs = model(inputs.float())
            _, preds = torch.max(outputs, 1)
            for t, p in zip(classes.view(-1), preds.view(-1)):
                    confusion_matrix[t.long(), p.long()] += 1
    
    class_accs = confusion_matrix.diag() / confusion_matrix.sum(1)
    class_acc_dict = dict(zip(class_names, class_accs))
    
    return class_acc_dict



def precision(model, data_loader, class_names : list):
    
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    
    model.eval()
    model.to(device)
    
    num_classes = len(class_names)
    
    confusion_matrix = torch.zeros(num_classes, num_classes)
    with torch.no_grad():
        for _, (inputs, classes) in tqdm(enumerate(data_loader)):
            inputs = inputs.to(device)
            classes = classes.to(device)
            outputs = model(inputs.float())
            _, preds = torch.max(outputs, 1)
            for t, p in zip(classes.view(-1), preds.view(-1)):
                    confusion_matrix[t.long(), p.long()] += 1
                    
    precision_ = confusion_matrix[0][0] / confusion_matrix[0, :].sum()
    
    return precision_
