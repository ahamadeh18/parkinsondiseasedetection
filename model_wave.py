# PyTorch imports:
import torch
import torch.nn as nn



class Model(nn.Module):
    def __init__(self, in_channels=1, num_classes=2, image_dim=(64, 64), dropout_prob=0.5):
        super(Model, self).__init__()
        
        # === Activation Function ===========================================================
        self.leaky_relu = nn.LeakyReLU(negative_slope=0.1)
        
        # === Dropout =======================================================================
        droput = nn.Dropout(p=dropout_prob)
    
        # === First Layer ===================================================================
        out_channel_1 = 64
        conv1_params = {'in_channels' : in_channels, 'out_channels' : out_channel_1, 'kernel_size' : 11,
                        'padding' : 3, 'stride' : 1}
        maxpool1_params = {'kernel_size' : 3, 'stride' : 3, 'padding' : 0}
        
        # === Second Layer ===================================================================
        out_channel_2 = 128
        conv2_params = {'in_channels' : out_channel_1,'out_channels' : out_channel_2, 'kernel_size' : 7,
                        'padding' : 1, 'stride' : 1}
        maxpool2_params = {'kernel_size' : 3, 'stride' : 3, 'padding' : 0}
        
        # === Third Layer ====================================================================
        out_channel_3 = 256
        conv3_params = {'in_channels' : out_channel_2,'out_channels' : out_channel_3, 'kernel_size' : 5,
                        'padding' : 1, 'stride' : 1}
        maxpool3_params = {'kernel_size' : 3, 'stride' : 3, 'padding' : 0}
        
        module_list = [
                    nn.Conv2d(**conv1_params),
                    nn.MaxPool2d(**maxpool1_params),
                    nn.BatchNorm2d(conv1_params['out_channels']),  
                    droput,
                    
                    nn.Conv2d(**conv2_params),
                    nn.MaxPool2d(**maxpool2_params),
                    nn.BatchNorm2d(conv2_params['out_channels']),
                    droput,
                    
                    nn.Conv2d(**conv3_params),
                    nn.MaxPool2d(**maxpool3_params),
                    nn.BatchNorm2d(conv3_params['out_channels']),
                    droput,
                    ]
        
        self.module_list = nn.ModuleList(module_list)
        
        for layer in self.module_list:
            if isinstance(layer, nn.Conv2d):
                nn.init.kaiming_normal_(layer.weight)
                nn.init.constant_(layer.bias, 0.0)
        
        image_size = image_dim
        
        for layer in self.module_list:
            if isinstance(layer, nn.Conv2d) or isinstance(layer, nn.MaxPool2d):
                image_size = self.new_image_dim(image_size, kernel_size=layer.kernel_size,
                                                padding=layer.padding, stride=layer.stride)
                
        in_features =  image_size[0] * image_size[1] *  module_list[-4].out_channels
        self.fc = nn.Linear(in_features=in_features, out_features=num_classes)
        
        
    def forward(self, x):
        
        for layer in self.module_list:
            x = layer(x)
            
        # === Fully Connected Layer ==========================================================
        # Flatten the feature maps to fit the fully-connected layer:
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        
        return x        
    
    
    def new_image_dim(self, image_dim : tuple, kernel_size : tuple, padding, stride):
        """ Finds the new dimensions of an input feature map after applying
        convolution or pooling.

        Args:
            image_dim (tuple): A tuple containing the dimension of the input 
            feature map.
            kernel_size (int): The kernel size of the filter.
            padding (int/tuple): The amount of padding to the feature map.
            stride (int/tuple): The stride (step size) of the filter.
        """
        if isinstance(padding, int):
            padding_height = padding_width = padding
        else:
            padding_height, padding_width = padding
            
        if isinstance(stride, int):
            stride_height = stride_width = stride
        else:
            stride_height, stride_width = stride

        if isinstance(kernel_size, int):
            kernel_height = kernel_width = kernel_size
        else:
            kernel_height, kernel_width = kernel_size
            
        new_image_height = ((image_dim[0] + 2 * padding_height - kernel_height) // stride_height) + 1
        new_image_width = ((image_dim[1] + 2 * padding_width - kernel_width) // stride_width) + 1
        
        return (new_image_height, new_image_width)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        