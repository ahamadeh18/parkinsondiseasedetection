# PyTorch imports:
from torch.utils.data import Dataset
from torchvision.datasets import ImageFolder
import torchvision.transforms as T

# NumPy imports:
import numpy as np

# Other imports:
import os
import json


DATA_PATHS = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data_path.txt")
DEFAULT_TRANSFORM = T.Compose([T.ToTensor(),
                               T.Grayscale(num_output_channels=1),
                               T.Resize((128, 128))])

class ParkinsonDataset(Dataset):
    def __init__(self, type : str, path=None, transform=DEFAULT_TRANSFORM):
        
        self.data_path = path        
        if path is None:
            self.type = type
            self.data_path = self.load_data_path()
        
        self.dataset = ImageFolder(self.data_path)
        self.classes = self.dataset.classes
        
        self.labels = self.dataset.targets
        
        self.mapping = self.dataset.class_to_idx
        self.rev_mapping = dict(zip(list(self.mapping.values()), list(self.mapping.keys())))
        self.transform = transform
    
    
    def load_data_path(self):
        
        paths_dict = None
        with open(DATA_PATHS, 'r') as file:
            paths_dict = json.loads(file.read())
            
        path = paths_dict[self.type]
        return path      
    
    
    def get_class_indecies(self, cls : int):
        class_inds = [i for i in range(len(self)) if self.labels[i] == cls]
        return class_inds

    
    def get_data_distribution(self):
        data_dist = np.bincount(self.labels)
        class_names = [self.rev_mapping[cls] for cls in np.unique(self.labels)]
        data_dist = dict(zip(class_names, data_dist))
        return data_dist
    
    
    def __getitem__(self, index):
        image = self.dataset[index][0]
        image = self.transform(image)        
        label = self.dataset[index][1]

        return image, label
    
    
    def __len__(self):
        return len(self.dataset)