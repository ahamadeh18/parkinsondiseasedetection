# PyTorch imports:
import torch
import torchvision
from torchvision.datasets import ImageFolder

# OpenCV imports:
import cv2

# NumPy imports:
import numpy as np

# Other imports:
import os
import json
# --- Constants -----------------------------------------------------------------------

data_path_json = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data_path.txt')

with open(data_path_json, 'r') as file:
    data_dict = json.loads(file.read())

SPIRAL_DATA_PATH = data_dict['raw_spiral']
WAVE_DATA_PATH = data_dict['raw_wave']

GENERATED_DATA_PATH = data_dict['save_path']

DEFAULT_NUM_LAYER = 4
IMAGE_EXTENSION = 'jpg'

CLASSES = ['healthy', 'parkinson']
CLASSES_MAPPING = {'healthy' : 0, 'parkinson' : 1}

IMAGE_SIZE = (256, 256, 3)

TRANSFORMS = [
            lambda image : np.array(image),
            lambda image, threshold, max_val, type : cv2.threshold(image, threshold, max_val, type)[1],
            lambda image : cv2.bitwise_not(image),
            lambda image, size: cv2.resize(image, size),
            lambda image, kernel, iterations: cv2.dilate(image, kernel, iterations),
             ]

TRANSFORMS_PARAMS = [{},
                    {'threshold' : 200, 'max_val' : 255, 'type' : cv2.THRESH_BINARY},
                    {},
                    {'size' : (256, 256)},
                    {'kernel' : np.ones((3, 3), np.uint8), 'iterations' : 1},
                    ]

MAX_DATASET_SIZE = 10000

# -------------------------------------------------------------------------------------
# --- Helper functions ----------------------------------------------------------------

def transform_image_alt(image):
    image = np.array(image)

    threshold_value = 175
    _, image = cv2.threshold(image, threshold_value, 255, cv2.THRESH_BINARY)
    image = cv2.bitwise_not(image)
    image = cv2.resize(image, (256, 256))
        
    kernel = np.ones((3, 3), np.uint8)
    image = cv2.dilate(image, kernel, iterations=1)
    
    return image

def transform_image(image, transforms, transforms_params):
    """Applies a series of transformations on a given image.

    Args:
        image (PIL): An input image to be transformed.
        transforms (List): A list containing the transformed to
        be applied on the given image.
        transforms_params (List): A list containing dictionaries
        that contain the parameter values for the transformations
        in the transforms list.

    Returns:
        N/A: An image transformed according to the given
        transformations.
    """
    for transform, params in zip(transforms, transforms_params):
        if len(params.values()) != 0:
            image = transform(image, **params)
        else:
            image = image = transform(image)
        
    return image


def generate_an_image(dataset, num_layers : int, transform=False, indecies=None):
    """Creates an image that consists of multiple images layered on top
    of each other.

    Args:
        dataset (_type_): The dataset the images will be created from.
        num_layers (int): The number of images to be put on top of 
        each other
        transform (bool, optional): If true the images will be transformed
        before being layered. Defaults to False.
        indecies (List, optional): If given, the layers will be sampled only
        from those indecies. Defaults to None.

    Returns:
        _type_: An image that consists of multiple images layered on top
        of each other.
    """
    if indecies is None:
        image_indecies = np.random.choice(list(range(len(dataset))), num_layers)
    else:
        image_indecies = np.random.choice(indecies, num_layers)
        
    layered_image = np.zeros(IMAGE_SIZE)
    
    for i in image_indecies:
        layer = dataset[i][0]
        
        if transform:
            layer = transform_image(layer,
                                    transforms=TRANSFORMS,
                                    transforms_params=TRANSFORMS_PARAMS)
        layered_image += layer
        
    return layered_image


def get_class_indecies(dataset, cls):
    """Given a dataset, returns the indecies of a given class
    in this dataset

    Args:
        dataset (_type_): The dataset.

    Returns:
        List: A list containing the indecies of a given class.
    """
    indecies = [i for i in range(len(dataset)) if dataset[i][1] == cls]
    return indecies


def create_dataset_folder(dataset_path : str, dataset_name : str, classes : list, dataset_type=None):
    """Creates a dataset folder with sub-folders according to the
    number of unique classes in the dataset.

    Args:
        dataset_path (str): The path of the dataset folder to be created
        classes (list): A list of the unique classes in the dataset.

    Returns:
        _type_: A dictionary with keys being the classes' names
        and values being the paths to the classes.
    """
    dataset_path = os.path.join(dataset_path, dataset_name)
    
    if not os.path.isdir(dataset_path):
        os.mkdir(dataset_path)
    
    if dataset_type is not None:
        dataset_path = os.path.join(dataset_path, dataset_type)
        if not os.path.isdir(dataset_path):
            os.mkdir(dataset_path)
    
    paths = {}
    for cls in classes:
        
        class_path = os.path.join(dataset_path, cls)
        if not os.path.isdir(class_path):
            os.mkdir(class_path)
            
        paths[cls] = class_path
    
    return paths        


def create_multiple_images(dataset, dataset_name, num_images : list, classes : list,
                           save_path=GENERATED_DATA_PATH, dataset_type=None, transform=True,
                           num_layers=DEFAULT_NUM_LAYER, classes_mapping=CLASSES_MAPPING):
    
    paths = create_dataset_folder(save_path, dataset_name, classes, dataset_type)
    
    for cls, num in zip(classes, num_images):
        indecies = get_class_indecies(dataset, classes_mapping[cls])
        for i in range(num):
            path = paths[cls]
            image = generate_an_image(dataset, num_layers=num_layers,
                                      transform=transform, indecies=indecies)
            
            image_name = f'{cls}_{i}.{IMAGE_EXTENSION}'
            path = os.path.join(path, image_name)
            cv2.imwrite(path, image)  
            
            
def validate_dataset_size(classes):
    new_class_sizes = []
    for cls in classes:
        if cls > MAX_DATASET_SIZE:
            new_class_sizes.append(MAX_DATASET_SIZE)
        else:
            new_class_sizes.append(cls)
    return new_class_sizes
# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------

def main():
    
    dataset_name = 'parkinson_dataset'
        
    # Creating the dataset with spiral images:
    num_healthy, num_parkinson = 7500, 7500
    num_healthy, num_parkinson = validate_dataset_size([num_healthy, num_parkinson])
    
    spiral_image_dataset = ImageFolder(SPIRAL_DATA_PATH)
    num_images = [num_healthy, num_parkinson]
    dataset_type = 'spiral'
    # create_multiple_images(spiral_image_dataset, dataset_name, num_images,
    #                        classes=CLASSES, dataset_type=dataset_type)

    # Creating the dataset with wave images:
    num_healthy, num_parkinson = 7500, 7500
    num_healthy, num_parkinson = validate_dataset_size([num_healthy, num_parkinson])
    
    wave_image_dataset = ImageFolder(WAVE_DATA_PATH)
    num_images = [num_healthy, num_parkinson]
    dataset_type = 'wave'
    # create_multiple_images(wave_image_dataset, dataset_name, num_images,
    #                        classes=CLASSES, dataset_type=dataset_type)
    

if __name__ == '__main__':
    main()