# This file generates some testing data from a dataset that is different
# from the one the model was trained on.

# PyTorch imports:
from torchvision.datasets import ImageFolder

# NumPy imports:
import numpy as np

# OpenCV imports:
import cv2

# Data generator imports:
from data_generator import transform_image, create_multiple_images

# Dataset imports:
from dataset import *

# Other imports:
import os
import json
import random


# --- Get the dataset path: --------------------------------------------------
data_path_json = 'data_path.txt'

json_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), data_path_json)

dataset_path = ''
with open(json_path, 'r') as file:
    dataset_path = json.loads(file.read())['demo_spiral']
# -----------------------------------------------------------------------------

# Creating the dataset object:
dataset = ImageFolder(dataset_path)

# Creating a random index to get a random image from the dataset:
index = random.randint(0, len(dataset))
index = 1

# Getting the image and its label:
image = dataset[index][0]
label = dataset[index][1]

# Getting the text label:
mapping = dict(zip(list(dataset.class_to_idx.values()), list(dataset.class_to_idx.keys())))

def mask_image(image):
    """ Applies several masks to the image in order to deniose it and make the drawn line
    better defined.

    Args:
        image (numpy.ndarray): The image where the masks are to be applied on.

    Returns:
        numpy.ndarray: The input image with the masks applied.
    """
    # 1) Making every pixel that is almost white white: This helps with further denoising the image. 
    image[image >= 200] = 255
    # 2) Making every pixel that is almost black white: This eliminates the black guideline in the
    #   original image as much as possible.
    image[image <= 62] = 255
    # 3) Making everything else white: Everything else in this context, is the desired spiral line.
    image[np.logical_and(image < 200,  image > 40)] = 0 
    
    return image


# Defining the image transformations which are:
#   1) Converting the image from PIL to numpy array with data type being unsigned integer.
#   2) Denoising the image:
#   3) Applying several masks to the image in order to deniose it and make the drawn line better defined.
#   4) Flipping the pixels so the desired line is white and the background is black.
#   5) Eroding the image: this gets rid of the black line in the original image.
#   6) Converting the image to grayscale: This makes converting the image to black and white possible.
#   7) Converting the image to black and white.
#   8) Dilating the image: This will connect the disconnected components and make the lines better defined.
TRANSFORMS = [
        lambda image: np.array(image, dtype=np.uint8),
        lambda image, dst, templateWindowSize, searchWindowSize, h, hcolor: cv2.fastNlMeansDenoisingColored(image, dst,
                                                                                                            templateWindowSize,
                                                                                                            searchWindowSize,
                                                                                                            h, hcolor),
        lambda image: mask_image(image),
        lambda image: cv2.bitwise_not(image),
        lambda image, kernel, iterations : cv2.erode(image, kernel, iterations),
        lambda image, color: cv2.cvtColor(image, color),
        lambda image, threshold, max_val, type : cv2.threshold(image, threshold, max_val, type)[1],
        lambda image, kernel, iterations: cv2.dilate(image, kernel, iterations),
        lambda image, size: cv2.resize(image, size)
]

TRANSFORMS_PARAMS = [
            {},
            {'dst': None, 'templateWindowSize': 7, 'searchWindowSize': 21,'h': 10, 'hcolor': 10},
            {},
            {},
            {'kernel': np.ones((4, 4)), 'iterations': 1},
            {'color': cv2.COLOR_RGB2GRAY},
            {'threshold' : 1, 'max_val': 255, 'type': cv2.THRESH_BINARY},
            {'kernel': np.ones((5, 5)), 'iterations': 5},
            {'size': (256, 256)}
                     ]

# for image, label in dataset:
    # print(image)

    # Tranforming the image:
    # image = transform_image(image, TRANSFORMS, TRANSFORMS_PARAMS)

    # label = mapping[label]
    # break
    # Plotting the image with its label:
    # cv2.imshow(label, image)
    # cv2.waitKey(1)


def main():

    dataset = ImageFolder(dataset_path)
    
    dataset_name = 'demo_spiral'
    num_images = [100, 100]
    classes = dataset.classes
    classes_mapping = dataset.class_to_idx
    
    create_multiple_images(dataset, dataset_name, num_images,
                        classes=classes, num_layers=4, classes_mapping=classes_mapping,
                        transforms=TRANSFORMS, transforms_params=TRANSFORMS_PARAMS)
    
if __name__ == "__main__":
    main()


