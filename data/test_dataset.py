from dataset import *
import random
import numpy as np
import cv2




dataset = ParkinsonDataset(type='spiral')

index = random.randint(0, len(dataset))


image, label = dataset[index]
label = dataset.rev_mapping[label]

print(label)

image = np.transpose(image.detach().cpu().numpy(), [1, 2, 0])

print(image.shape)

cv2.imshow(label, image)
cv2.waitKey(0)

# image = np.sum(image, axis=2)[:, :, np.newaxis]
# print(image.shape)
# cv2.imshow(label, image)
# cv2.waitKey(0)