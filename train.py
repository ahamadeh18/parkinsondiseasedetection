# PyTorch imports:
import torch

# Other imports:
from tqdm import tqdm
import time

# Evaluation imports:
from evaluate import evaluate


def train(model, train_loader, validation_loader, optimizer, criterion, epochs, lr_scheduler=None, verbose=False):
    """ The training routine that will be used for the trianing the model.

    Args:
        model (nn.Module): The model to train.
        train_loader (torch.utils.data.DataLoader): The DataLoader object
            that contains the training data
        criterion: The loss function to be used for training.
        optimizer: The optimizer to be used for training.
        epochs (int): The number of training epochs
        lr_scheduler: The learning rate scheduler
        verbose (bool, optional): If true, the function prints details about
            the training procedure. Defaults to False.

    Returns:
        total_loss (List): A list containing the numerical loss value
            of each epoch during the training.
        train_accuracies (List): A list containing the accuracy of the
            model for each epoch during the training.
        val_accuracies (List): A list containg the accuracy of the model
            for each epoch during the training.
    """

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    
    # Setting the model to the training mode:
    model.train()
    # Moving the model to the desired device:
    model = model.to(device)
    
    total_loss = []
    train_accuracies = []
    val_accuracies = []
    
    for epoch in range(epochs):
        
        epoch_loss = 0
        epoch_tic = time.time()        
        
        for x, y in tqdm(train_loader):
            # Moving the data and the labels to the desired device:            
            x = x.to(device)
            y = y.to(device)
            
            # Setting the optimizer to zero_grad in order to reset
            # the gradients so each batch will have its seperate
            # gradients:
            optimizer.zero_grad()
            
            # Generating predictions:
            y_pred = model(x)
            
            # Calculating the loss:
            loss = criterion(y_pred, y)
            
            # Accumulating the loss:
            epoch_loss += loss.item()
            
            # Back propagation
            loss.backward()
            
            # Stepping the optimizer:
            optimizer.step()
        
        epoch_toc = time.time()
        epoch_runtime = epoch_toc - epoch_tic
        
        total_loss.append(epoch_loss)
        
        print('Evaluating epoch...')
        
        train_accuracy = evaluate(model, train_loader)
        val_accuracy = evaluate(model, validation_loader)
        
        train_accuracies.append(train_accuracy)
        val_accuracies.append(val_accuracy)
        
        if lr_scheduler is not None:
            # Getting the current learning rate from the optimizer:
            lr = lr_scheduler.param_groups[0]['lr']
            print(f'Current learning rate: {lr}')
            
            # Advancing the learning rate scheduler:
            lr_scheduler.step()
        
        if verbose:
            print(f'Epoch: {epoch} | Train acc: {train_accuracy:.2f}% | Val acc: {val_accuracy:.2f}% \
| Loss: {epoch_loss:.4f} | Runtime: {epoch_runtime:.2f} seconds or {epoch_runtime / 60:.2f} mins')
            
    return total_loss, train_accuracies, val_accuracies
            
        
    
    