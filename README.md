# ParkinsonDiseaseDetection

## I. Introduction:
Parkinsion's disease detection is a model that detects whether someone has the Parkinson's disease or not. This detection is based on image inputs from a user with these inputs being:
1. 4 images of spirals.
2. 4 images of waves.

Once the model recieves the inputs it sends the spiral images to a model trained on spiral images and the wave images to a model that is trained on wave images.
Each model (spiral/wave) provides a score to its inputs and then the scores are added together for the final prediction. The final prediction is based on applying argmax to the added scores from both models.

## II. User Input:
The user can input these images by drawing them into the applications. This could be done it at least two ways which are:
1. Drawing the images on a canvas integrated into the application.
2. The application tracks the users index finger and guides the user to move their hand in a spiral and wave paths.

**NOTE:** The second option is better for generating more accurate results since drawing on a mobile could provide support to the finger/hand because the finger would be resting on the screen of the device.

Ideally, the sequence of the input and generating predictions should be as follows:
1. The application prompts the user to draw 4 spirals.
2. The user draws the 4 spirals either by drawing them on the device, or by moving their hand in a spiral path (assuming that the application has a finger-tracking model.)
3. The drawn spirals are layered on top of each other (This has to do with how the model was train refer to the dataset section for more information) and then fed to the spiral model to generate the scores.
4. The application prompts the user to draw 4 waves.
5. The user draws the 4 waves either by drawing them on the device, or by moving their hand in a wave path (assuming that the application has a finger-tracking model.)
6. The drawn waves are layered on top of each other and then fed to the wave model to generate the scores.
7. The spiral and wave scores are added together.
8. The final prediction is generated according to the scores from both models and is shown to the user.

## III. The Models' Architecutres:

### 1. The Spiral Model:
This model consists of 3 convolutional layers and a fully-connected layer:

#### 1.a. The first layer:
* Convolution operation with a kernel size of 7 by 7, a padding of 2, and a stride of 2. There are 32 of this filter/convolution.
* Max-pooling of kernel size of 3 by 3, a padding of 0, and a stride of 3
* Batch normalization.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with probability of 50%

#### 1.b. The second layer:
* Convolution operation with a kernel size of 5 by 5, a padding of 1, and a stride of 1. There are 64 of this filter/convolution.
* Max-pooling of kernel size of 3 by 3, a padding of 0, and a stride of 3
* Batch normalization.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with probability of 50%

#### 1.c. The third layer:
* Convolution operation with a kernel size of 3 by 3, a padding of 1, and a stride of 1. There are 64 of this filter/convolution.
* Max-pooling of kernel size of 3 by 3, a padding of 0, and a stride of 3
* Batch normalization.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with probability of 50%

#### 1.d. The fully-connected layer:
In this layer, the output of the previous convolutional layer is flattened into a single vector and fed to a linear layer which has an output size of 2.

#### 1.f. Weight Initialization:
The weigths of all of the convolutional layers were initialized using the normalized kaiming method since all of these layers have leaky ReLU as an activation function.


### 2. The Wave Model:

#### 2.a. The first layer:
* Convolution operation with a kernel size of 11 by 11, a padding of 3, and a stride of 1. There are 64 of this filter/convolution.
* Max-pooling of kernel size of 3 by 3, a padding of 0, and a stride of 3
* Batch normalization.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with probability of 50%

#### 2.b. The second layer:
* Convolution operation with a kernel size of 7 by 7, a padding of 1, and a stride of 1. There are 128 of this filter/convolution.
* Max-pooling of kernel size of 3 by 3, a padding of 0, and a stride of 3
* Batch normalization.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with probability of 50%

#### 2.c. The third layer:
* Convolution operation with a kernel size of 5 by 5, a padding of 1, and a stride of 1. There are 258 of this filter/convolution.
* Max-pooling of kernel size of 3 by 3, a padding of 0, and a stride of 3
* Batch normalization.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with probability of 50%

#### 2.d. The fully-connected layer:
In this layer, the output of the previous convolutional layer is flattened into a single vector and fed to a linear layer which has an output size of 2.

#### 2.f. Weight Initialization:
The weigths of all of the convolutional layers were initialized using the normalized kaiming method since all of these layers have leaky ReLU as an activation function.


## IV. Dataset:
The dataset was created according to the following dataset: [Parkinson's Drawings](https://www.kaggle.com/datasets/kmader/parkinsons-drawings). The dataset was created as follows:
Since the original dataset was too small (Had only 102 images) to train a reliable model on, a new dataset based on those 102 images was created by:
Layering 4 images on top of each other to create a new image. This allowed creating thousand of images, however, it forces the model to only accept 4 layered images as input. It it worth noting that before creating the images all the original images were pre-processed.

### Pre-processing:
The pre-processing consists of of the following:
1. Apply thresholding on the image by making every pixel that has a value greater than 200 a white pixel (of value 255.) This eliminates the noisy background of the images and highlights the shape in the image (whether it is spiral or wave.)
2. The pixel values are flipped in order to make the shape be in white and the background black.
3. The images are resized to 256 by 256. This helps a lot since the images in the original dataset do not have uniform dimensions.
4. A 3 by 3 dilation filter is applied to the images in order to make the shapes more defined, since in the original dataset, the shapes are drawing with pens/pencils which means the outlines of the shapes is very narrow, so dilation helps a lot making the shapes better defined.
The figures below demonstrate how the pre-processing changes the original images:

|![Original spiral image](/figures/spiral_original.png "Original spiral image")|![Pre-processed spiral image](/figures/spiral_preprocessed.png "Pre-processed spiral image")|
|:---------------:|:---------------:|
| *Original spiral image* | *Pre-processed spiral image* | 

|![Original wave image](/figures/wave_original.png "Original wave image")|![Pre-preocessed wave image](/figures/wave_preprocessed.png "Pre-processed wave image")|
|:---------------:|:---------------:|
| *Original wave image* | *Pre-processed wave image* | 

### Spiral Dataset:
For the spiral images, 7500 images were created for both healthy and patient images which totals to 15000 images.

### Wave Dataset:
For the wave images, also 7500 images were created for both healthy and patient images which also totals to 15000 images.

## V Training the models:
Before training any model, both spiral and wave Dataset objects were inputed a transform that applies the following transformations on the images:
* Convert the images to torch.Tensor.
* Convert the images to grayscale with a one channel (as opposed to having 3 channels which are RGB) 
* Re-size the images to 128.

### 1. The Spiral model:
#### The training routine:
* **Data loaders:** Three data loader (torch.utils.data.DataLoader) objects were created for each data split (train, validation, and test) with the splits being 70% for training 15% for validation and 15% for testing with a number of workers of 6 and a batch size of 8 all of the data loaders.
* **The optimzer:** Adam optimizer with initial learning rate of 1e-5.
* **The loss function:** Cross entropy loss
* **Number of epochs:** The model was trained for 5 epochs

### 2. The Wave model:
#### The training routine:
* **Data loaders:** Three data loader (torch.utils.data.DataLoader) objects were created for each data split (train, validation, and test) with the splits being 60% for training 20% for validation and 20% for testing with a number of workers of 6 and a batch size of 8 all of the data loaders.
* **The optimzer:** Stochastic Gradient Descent optimizer with initial learning rate of 8e-6 and momentum of 0.9.
* **The loss function:** Cross entropy loss
* **Number of epochs:** The model was trained for 7 epochs

## VI Results:

### 1. The Spiral model:
Reached an accuracy of 98.98%, 97.64%, and 98.62% on the train, validation and test sets recpectively.

### 2. The Wave model:
Reached an accuracy of 99.33%, 99.33%, and 99.17% on the train, validation and test sets recpectively.

### 3. The joint models:
The joint model consists of both models generating scores, then the scores are added together and finally, an argmax is applied on the joint scores to generate the final prediction.
This model reached an accuracy of 100.00% when tested with a subset of 1500 images from the original dataset and accuracy of ~99.00% when tested with the whole dataset.

# IMPORTANT NOTE:
It is important to include a .txt file in the data folder with the name "data_paths.txt" which contains the path to the data.
The file format should be as follows:
<pre><code>{
    "spiral" : "[The path to the generated spiral data]",
    "wave" : "[The path to the generated wave data]",
    "raw_spiral" : "[The path to the original spiral data]",
    "raw_wave" : "[The path to the original wave data]",
    "save_path" : "[The path where the generated data will be saved]"
}

